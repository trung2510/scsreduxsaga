import React from 'react';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga'
import allReducers from './src/redux/allReducers';
import rootSaga from './src/redux/rootSaga';
import { RootScreen } from './src/screens/RootScreen';

const sagaMiddleware = createSagaMiddleware();

let store = createStore(allReducers, applyMiddleware(sagaMiddleware))

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootScreen/>
      </Provider>
    );
  }

};

sagaMiddleware.run(rootSaga);




